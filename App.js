
import { 
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  SafeAreaView,
  Touch,
  Button,
  Alert,
  Touchable
} from 'react-native';
import { useFonts, Righteous, Righteous_400Regular } from '@expo-google-fonts/righteous';
import { StatusBar } from 'expo-status-bar';
import LoginImage from './assets/image-login.png';
import AppLoading from 'expo-app-loading';
import { TouchableOpacity } from 'react-native-web';

export default function App() {
/*  let [fontsLoaded] = useFonts({
    Righteous_400Regular,
  });

  let fontSize = 24;
  let paddingVertical = 6;
  if (!fontsLoaded) {
    return <AppLoading />;
  
  } else {
*/
  return (
    <View style={styles.container}> 
      <Text style={styles.texto_h1Login} >TraineLog</Text>
      <Text style={styles.texto_h2Login}>Equipe Developer</Text>
      
      <Text style={styles.retangulo}> </Text>
      <Image style={styles.Ilogin} source={LoginImage}/>
      <StatusBar style='auto' />
      <View>
    <SafeAreaView style={styles.InitialButton}>
    <TouchableOpacity>
      <Text style={{ color: 'white', fontSize: 32, fontFamily: 'Righteous_400Regular'}}>Criar conta</Text>
      </TouchableOpacity>
      </SafeAreaView>
      <br/>
      <SafeAreaView style={styles.InitialButton}>
      <Text style={{ color: 'white', fontSize: 32, fontFamily: 'Righteous_400Regular'}}>Fazer Login</Text>
      </SafeAreaView>
      </View> 
    </View>

    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
  },

  texto_h1Login: {
    fontWeight: 'bold',
    Weight: 700,
    padding: 3,
    size: 19,
    lineHeight: 22.33,
    color: 'rgb(33, 137, 126)'
  },

  texto_h2Login: {
    Weight: 300,
    size: 17,
    lineHeight: 19.98,
    color: '#2DBAB1'
  },

  retangulo: {
    padding: 85,
    marginBottom: 50,
    width:"85%",
    marginTop: 50,
    borderRadius: 80,
    backgroundColor: '#0610FF'
  },

  Ilogin: {
    width: 190,
    height: 200,  
    bottom: 240,
    marginEnd: 25
  },
  InitialButton: {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#2DBAB1',
  width: 203,
  height: 64,
  borderRadius: 30
  }
});


